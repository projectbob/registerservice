/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * RegisterServiceImpl.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.register.service;

import com.tgetzoya.projectbob.models.RegisterModel;
import com.tgetzoya.projectbob.models.ResponseModel;
import com.tgetzoya.projectbob.models.UserModel;
import com.tgetzoya.projectbob.models.error.ErrorModel;
import com.tgetzoya.projectbob.utils.password.PasswordUtils;
import com.tgetzoya.projectbob.utils.salt.SaltUtils;
import com.tgetzoya.projectbob.utils.types.DefaultTypes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.env.Environment;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;

import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;
import java.security.NoSuchAlgorithmException;
import java.util.Arrays;
import java.util.UUID;

@Component
@PropertySource("classpath:locations.properties")
public class RegisterServiceImpl implements RegisterService {
    /**
     * To get the urls from locations.properties file.
     */
    @Autowired
    Environment mEnv;

    /**
     * Validates user input, then creates a new user object.  Once done, it
     * passes the information off the Read/Write service.
     *
     * @param registerModel the data supplied by the end user
     * @return UserModel with account information
     */
    @Override
    public ResponseEntity registerUser(RegisterModel registerModel) {
        RestTemplate template = new RestTemplate();

        ResponseEntity<ResponseModel> response = template.postForEntity(
                mEnv.getProperty("url.validation.registration"),
                registerModel,
                ResponseModel.class);

        if (response.getBody().getResult() != 0) {
            return response;
        }

        UserModel userModel = new UserModel();
        userModel.setAddress(registerModel.getAddress());
        userModel.setId(UUID.nameUUIDFromBytes(registerModel.getEmail().getBytes()));
        userModel.setName(registerModel.getName());
        userModel.setEmail(registerModel.getEmail());

        ByteBuffer salt;
        ByteBuffer pass;

        try {
            salt = SaltUtils.generateSalt();
            pass = PasswordUtils.generatePasswordHash(
                    ByteBuffer.wrap(registerModel.getPassword().getBytes(DefaultTypes.STRING_ENC_TYPE)),
                    salt);
        } catch (NoSuchAlgorithmException | UnsupportedEncodingException e) {
            ErrorModel em = new ErrorModel("Could not create password.");
            ResponseModel rm = new ResponseModel();
            rm.setResult(1);
            rm.setErrors(Arrays.asList(em));

            return ResponseEntity.ok(rm);
        }

        userModel.setPassword(pass);
        userModel.setSalt(salt);

        response = template.postForEntity(
                mEnv.getProperty("url.readwrite.createuser"),
                userModel,
                ResponseModel.class);

        return response;
    }

    /**
     * Returns a status of OK if the service is running.
     *
     * @return a status of OK if the service is running
     */
    @Override
    public ResponseEntity healthCheck() {
        return ResponseEntity.ok("{\"status\":\"ok\"}");
    }
}

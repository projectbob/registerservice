/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * RegisterService.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.register.service;


import com.tgetzoya.projectbob.models.RegisterModel;
import org.springframework.http.ResponseEntity;

/**
 * Interface for registering a new user.
 */
public interface RegisterService {
    /**
     * Registers a new user using the input supplied by the end user.
     *
     * @param registerModel the data supplied by the end user
     * @return a UserModel with account information
     */
    ResponseEntity registerUser(RegisterModel registerModel);

    /**
     * Returns a status of OK if the service is up and running.
     *
     * @return A status of OK if the service is running
     */
    ResponseEntity healthCheck();
}

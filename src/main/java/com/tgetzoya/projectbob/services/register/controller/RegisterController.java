/* *****************************************************************************
 * *****************************************************************************
 * *****************************************************************************
 * RegisterController.java
 * (c) 2016 Thomas Getzoyan <tgetzoya@gmail.com>
 * All Rights Reserved
 * *****************************************************************************
 * *****************************************************************************
 * ****************************************************************************/
package com.tgetzoya.projectbob.services.register.controller;

import com.tgetzoya.projectbob.models.RegisterModel;
import com.tgetzoya.projectbob.services.register.service.RegisterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * This controller handles all user registration endpoints.
 *
 * @author Thomas Getzoyan <tgetzoya@gmail.com>
 * @version 1.0
 * @since 1.0
 */
@RestController
public class RegisterController {
    /**
     * The class that will do the actual work.
     */
    @Autowired
    private RegisterService registerService;

    /**
     * Returns the string "OK" if the service is working.
     *
     * @return String "OK" if it's working
     */
    @RequestMapping(value = "/healthCheck",
            method = RequestMethod.GET,
            produces = MediaType.TEXT_XML_VALUE)
    @ResponseBody
    public ResponseEntity healthCheck() {
        return registerService.healthCheck();
    }

    /**
     * Takes a {@link RegisterModel} as input and will return the new user id
     * on success.
     *
     * @param registerModel the user input for registering a new user
     * @return the ResponseEntity with the result
     */
    @RequestMapping(value = "/user",
            method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public ResponseEntity register(@RequestBody RegisterModel registerModel) {
        return registerService.registerUser(registerModel);
    }
}
